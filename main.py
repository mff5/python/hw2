#!/bin/python

OPS = {
    "+": lambda x, y: x + y,
    "-": lambda x, y: x - y,
    "*": lambda x, y: x * y,
    "/": lambda x, y: x // y,
}


def listify_input(str_input: str):
    return [i for i in str_input.split() if i]


def handle_item(item, stack):
    if item in OPS:
        if len(stack) < 2:
            raise ValueError
        second_op = stack.pop()
        first_op = stack.pop()
        stack.append(OPS[item](first_op, second_op))
    else:
        number = int(item)
        stack.append(number)


def print_stack(stack):
    if len(stack) > 0:
        print(stack[-1])


def main():
    while True:
        stack = []
        try:
            input_list = listify_input(input())
            for list_item in input_list:
                handle_item(list_item, stack)
            print_stack(stack)
        except EOFError:
            # End-Of-File, program should return now
            break
        except ValueError:
            print("Malformed expression")
        except ZeroDivisionError:
            print("Zero division")


if __name__ == "__main__":
    main()
